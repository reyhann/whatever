package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class anasayfa extends AppCompatActivity {

    private Button btngramer,btnsarki;
    public void init(){
        btngramer=(Button)findViewById(R.id.btngramer);
        btnsarki=(Button)findViewById(R.id.btnsarki);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anasayfa);
        init();
        btngramer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentgramer=new Intent(anasayfa.this,gramer.class);
                startActivity(intentgramer);

            }
        });
        btnsarki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentsarki=new Intent(anasayfa.this,sarkiActivity.class);
                startActivity(intentsarki);

            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);

        if(item.getItemId()== R.id.mainLogout){

            Intent loginIntent=new Intent(anasayfa.this,LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }
        return true;

    }

}