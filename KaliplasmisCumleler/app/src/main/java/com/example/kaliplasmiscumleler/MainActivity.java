package com.example.kaliplasmiscumleler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

public class MainActivity extends AppCompatActivity {

  CardView ing1,ing2,ing3;
    private Object ViewFlipper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        android.widget.ViewFlipper flipper;
        ViewFlipper= flipper =(ViewFlipper) findViewById(R.id.viewFlipper);
        ing1=(CardView) findViewById(R.id.ing1);
        ing2=(CardView) findViewById(R.id.ing2);
        ing3=(CardView) findViewById(R.id.ing3);

        ing1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this,KaliplasmisCumleler.class);
                myIntent.putExtra("ingid",1);
                startActivity(myIntent);
            }
        });
        ing2.setOnClickListener(new View.OnClickListener() {
            @Override
        public void onClick(View v) {
            Intent myIntent = new Intent(MainActivity.this,KaliplasmisCumleler.class);
            myIntent.putExtra("ingid",2);
            startActivity(myIntent);
        }
    });
        ing3.setOnClickListener(new View.OnClickListener() {
            @Override
    public void onClick(View v) {
        Intent myIntent = new Intent(MainActivity.this,KaliplasmisCumleler.class);
        myIntent.putExtra("ingid",3);
        startActivity(myIntent);
    }
});
        flipper.setAutoStart(true);

    }
}