package com.example.kaliplasmiscumleler;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class KaliplasmisCumleler extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kaliplasmis_cumleler);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView klp=(TextView) findViewById(R.id.klp);
        ImageView resim=(ImageView) findViewById(R.id.resimx);

        Intent mIntent=getIntent();
        int ingid=mIntent.getIntExtra("ingid",0);

        if(ingid==1){
            resim.setBackground(getResources().getDrawable(R.drawable.ing));
            klp.setText("\n" +
                    "1- What’s up? – Naber?\n" +
                    "2- So so – İdare eder.\n" +
                    "3- How is it going? – Nasıl gidiyor?\n" +
                    "4- Let’s go! – Haydi gidelim!\n" +
                    "5- Let’s go on! – Devam edelim!\n" +
                    "6- Give me a hand! – Yardım et, bir el at!\n" +
                    "7- I will see to it! – İlgileneceğim, bakacağım.\n" +
                    "8- Get out of here! – Defol buradan!\n" +
                    "9- My head is killing me! – Başım beni öldürüyor!\n" +
                    "10- What a beatiful girl! – Ne güzel bir kız!\n" +
                    "11- I am broke! – Parasızım!\n" +
                    "12- I am hungry! – Açım!\n" +
                    "13- I am starving! – Açlıktan ölüyorum!\n" +
                    "14- Don’t get me wrong! – Beni yanlış anlama!\n" +
                    "15- It sounds good! – Kulağa hoş geliyor!\n" +
                    "16- It is beyond me! – Bu beni aşar!\n" +
                    "17- It is up to you! – Sana bağlı, sen bilirsin.\n" +
                    "18- You can trust me!– Bana güvenebilirsin!\n" +
                    "19- Lets get to the point! – Sadede gelelim!\n" +
                    "20- Guess what! – Bil bakalım!\n" );

        }
else if(ingid==2){
            resim.setBackground(getResources().getDrawable(R.drawable.ingg));
            klp.setText("\n" +
                    "21- I am out of condition! – Formdan düştüm!\n" +
                    "22- My battery is dead. – Şarjım bitti.\n" +
                    "23- My battery is low. – Şarjım az kaldı.\n" +
                    "24- You know what! – Ne diyeceğimi biliyorsun!\n" +
                    "25- It is better than nothing! – Hiç yoktan iyidir!\n" +
                    "26- I’ll get the phone/door! – Kapıya, telefona ben bakarım.\n" +
                    "27- It’s my turn! – Benim sıram!\n" +
                    "28- It doesn’t matter! – Sorun değil!\n" +
                    "29- You are welcome. – Rica ederim.\n" +
                    "30- Long time no see! – Görüşmeyeli uzun zaman oldu!\n" +
                    "31- Long time no talk! – Konuşmayalı uzun zaman oldu!\n" +
                    "32- Do you wanna hangout? – Takılmak ister misin?\n" +
                    "33- Let’s hangout tonigh!– Bu gece buluşalım.\n" +
                    "34- I hope so! – Öyle umuyorum.\n" +
                    "35- I have no idea! – Hiçbir fikrim yok!\n"+
                    "36- Let’s meet! – Buluşalım.\n" +
                    "37- You don’t look your age!– Yaşını göstermiyorsun!\n" +
                    "38- We are safe and sound! – Güvendeyiz.\n" +
                    "39- Don’t piss me of! – Beni kızdırma!\n" +
                    "40- Get well soon! – Geçmiş olsun!\n"

                   );
        }
else if(ingid==3){
            resim.setBackground(getResources().getDrawable(R.drawable.inggg));
            klp.setText("\n" +
                    "41- I lost my temper! – Sinirlerim bozuldu!\n" +
                    "42- Make yourself at home!– Kendini evinde hisset!\n" +
                    "43- Hurry up! – Acele et!\n" +
                    "44- Calm down!– Sakin ol!\n" +
                    "45- Help yourself. – Buyrun?\n" +
                    "46- In my opinion… – Benim düşünceme göre…\n" +
                    "47- I dont mind!– Benim için fark etmez!\n" +
                    "48- Do you know each other? – Birbirinizi tanıyor musunuz?\n" +
                    "49- Are you on facebook? – Facebook’un var mı?\n" +
                    "50- What brings you to here? – Seni buraya getiren nedir?\n" +
                    "51- Dont blame me! – Beni suçlama!\n" +
                    "52- I am in love her/him! – Ona aşığım.\n" +
                    "53- I am in touch with Sally! – Sally ile hala görüşüyorum.\n" +
                    "54- I have an idea. – Bir fikrim var.\n" +
                    "55- I have hands full now. – Şu an ellerim dolu.\n" +
                    "56- My room is an a mess. – Odam çok dağınık.\n" +
                    "57- Are you kidding? – Şaka mı yapıyorsun?\n" +
                    "58- As soon as possible – Mümkün olan en kısa sürede.\n" +
                    "59- By the way– Bu arada\n" +
                    "60- Do me a favor – Bana bir iyilik yap.\n" );

        }
    }
}