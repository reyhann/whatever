package com.example.splashscreenwithlogin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.widget.EditText;
import android.widget.Toast;
public class LoginActivity extends AppCompatActivity {
    static String usernameString = "ceyda";
    static String passwordString = "123";
    EditText username;
    EditText password;

    private Button btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.pass);
        btnlogin = (Button) findViewById(R.id.btnlogin);


        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //edittextlere girilen degerleri, kodun okunabilirligini arttirmak icin degiskenlerimde tutuyorum.
                String getusername = username.getText().toString();
                String getpassword = password.getText().toString();

                //Kullanici adi VE sifre eslesiyorsa
                if (usernameString.equals(getusername) && passwordString.equals(getpassword)) {
                    //Yeni activity'e gecis saglansin
                    Intent myIntent = new Intent(LoginActivity.this, bos.class);

                    myIntent.putExtra("username", getusername); //Optional parameters
                    LoginActivity.this.startActivity(myIntent);

                }
                //Kullanici adi VEYA sifre eslesmiyorsa
                else {
                    Toast.makeText(LoginActivity.this, "Kullanıcı adı veya şifre yanlış", Toast.LENGTH_SHORT).show();
                }

            }
        });}


    }

