package com.muhammedcamlibel.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }


    public void dogru(View view ){
        Toast.makeText(getApplicationContext(),"Tebrikler!!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Main2Activity.this,Main3Activity.class);
        startActivity(intent);
    }

    public void yanlis(View view){

        Toast.makeText(getApplicationContext(),"Yanlış Cevap!!",Toast.LENGTH_SHORT).show();
    }

}
