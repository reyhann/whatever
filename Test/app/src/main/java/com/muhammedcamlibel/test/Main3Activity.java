package com.muhammedcamlibel.test;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }

    public void dogru(View view ){

        Toast.makeText(getApplicationContext(),"TEBRİKLER TEST BİTTİ !!",Toast.LENGTH_SHORT).show();





        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Restart");
        alert.setMessage("Are you sure ?");
        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(Main3Activity.this,MainActivity.class);
                startActivity(intent);

            }
        });

        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Notsaved
                Toast.makeText(getApplicationContext(),"BY BY",Toast.LENGTH_LONG).show();
            }
        });

        alert.show();


    }

    public void yanlis(View view){

        Toast.makeText(getApplicationContext(),"Yanlış Cevap!!",Toast.LENGTH_SHORT).show();
    }


}
