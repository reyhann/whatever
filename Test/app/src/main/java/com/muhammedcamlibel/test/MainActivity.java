package com.muhammedcamlibel.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ImageView imageView ;
    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);


    }



    public void dogru(View view ){
        Toast.makeText(getApplicationContext(),"Tebrikler!!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MainActivity.this,Main2Activity.class);
        startActivity(intent);
    }

    public void yanlis(View view){

        Toast.makeText(getApplicationContext(),"Yanlış Cevap!!",Toast.LENGTH_SHORT).show();
    }

}
