package com.example.notdefteri;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Not extends AppCompatActivity {
    SQLiteDatabase database;
    TextView editText;
    TextView baslıkText;
    String yorum="";
    String baslik="";
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not); editText = findViewById(R.id.editText);
        button = findViewById(R.id.button2);
        baslıkText = findViewById(R.id.editText2);


        database = this.openOrCreateDatabase("Veritabani",MODE_PRIVATE,null);


        Intent intent = getIntent();
        String info = intent.getStringExtra("info");

        if (info.equals("new")) {
            editText.setText("");
            baslıkText.setText("");
            button.setVisibility(View.VISIBLE);
        } else {

            int idyorum = intent.getIntExtra("Id", 1);
            button.setVisibility(View.INVISIBLE);
            try {
                Cursor cursor = database.rawQuery("SELECT * FROM veritabani WHERE id = ?", new String[]{String.valueOf(idyorum)});
                int Ix = cursor.getColumnIndex("yorum");
                int Ix1 = cursor.getColumnIndex("baslik");


                while (cursor.moveToNext()) {
                    editText.setText(cursor.getString(Ix));
                    baslıkText.setText(cursor.getString(Ix1));

                }

                cursor.close();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }

    public void yorumEkle(View view){

        yorum = editText.getText().toString();
        baslik = baslıkText.getText().toString();


        try {

            database = this.openOrCreateDatabase("Veritabani",MODE_PRIVATE,null);
            database.execSQL("CREATE TABLE IF NOT EXISTS veritabani (id INTEGER PRIMARY KEY, yorum VARCHAR , baslik VARCHAR)");
            String sqlString ="INSERT INTO veritabani(yorum , baslik) VALUES (?,?)";
            SQLiteStatement sqLiteStatement = database.compileStatement(sqlString);
            sqLiteStatement.bindString(1,yorum);
            sqLiteStatement.bindString(2,baslik);
            sqLiteStatement.execute();

        }catch (Exception e){
            e.printStackTrace();
        }

        Intent intent = new Intent(Not.this, NotEkle.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);


    }




}
