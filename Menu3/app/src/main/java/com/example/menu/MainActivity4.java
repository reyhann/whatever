package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity4 extends AppCompatActivity {
    ListView listView;
    ArrayList<String> list;
    ArrayAdapter adapter;
    String kelime="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        listView = findViewById(R.id.listView1);

        list = new ArrayList<String>();
        list.add(" ");
        list.add("   A");
        list.add("   B");
        list.add("   C");
        list.add("   D");
        list.add("   E");
        list.add("   F");
        list.add("   G");
        list.add("   H");
        list.add("   I-i");
        list.add("   J");
        list.add("   K");
        list.add("   L");


        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity4.this,MainActivity2.class);
                intent.putExtra("bilgi",list.get(position));


                startActivity(intent);

            }
        });


    }
}
