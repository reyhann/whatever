package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.HashMap;

public class HikayeAc extends AppCompatActivity {
    TextView textView;
    HashMap<String, String> hashMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hikaye2);
        textView = findViewById(R.id.textView);


        hashMap = new HashMap<>();
        hashMap.put(" ", " ");
        hashMap.put("   Blue Bird"," \n Adam is walking. He sees a bluebird up in the tree. He wonders what it could be.\n" +
                "\n" +
                "(Adam yürürken ağacın içinde mavi bir kuş görüyor. Ne olabileceğini merak ediyor.)\n" +
                "\n" +
                "He opens his phone and searches it up. It’s a blue jay. Adam has never seen a blue jay before.\n" +
                "\n" +
                "(Telefonunu açar ve arar. Mavi bir alakargadır. Adam daha önce mavi bir alakarga görmemiştir.)\n" +
                "\n" +
                "He takes a picture. The bird flies off. Adam will never forget the blue bird he saw.\n" +
                "\n" +
                "(Adam fotoğrafını çekiyor. Kuş uçuyor. Adam, gördüğü mavi kuşu asla unutmayacaktır.)");
        hashMap.put("   DisneyWorld"," I live near Disneyworld. It is 20 minutes away." +"\n" +
                "(Disneyworld yakınlarında yaşıyorum. 20 dakikalık uzaklıktadır.)\n"+

                "I go every summer when the weather is right. I map out the parks and the rides.\n" +

                "(Her yaz havalar iyi olduğu zaman gidiyorum. Parkları ve gezinti yollarını haritalıyorum.)\n" +
                "I have free pass. Because I won them in a contest.\n" +
                "(Bedava geçiş hakkım var. Çünkü onları bir yarışmada kazandım.)\n"  +
                "I take pictures with the Mickey and Goofy. And I take pictures with my family.");
        hashMap.put("   My Best Friend"," My Best friend and I love to play." +"\n" +
                "(Arkadaşım ve ben oyun oynamayı severiz.)\n" +
                "We play just about every day.\n" +
                "(Neredeyse her gün oyun oynarız.)\n" +
                "Our favorite game is basketball.\n" +
                "(Basketbol bizim en sevdiğimiz oyundur.)\n" +
                "We throw the ball and bounce it around.\n" +
                "(Etrafta topu fırlatır ve zıplatırız.)\n" +
                "And kick it every once in a while.\n" +
                "(Ve her seferinde topa  vururuz.)\n" +
                "On the court and off the court.\n" +
                "(Sahada ve sahanın dışına)\n" +
                "My best friend loves to win.\n" +
                "(En iyi arkadaşım kazanmaya bayılır.)\n" +
                "He scores every time.\n" +
                "(Her seferinde basket atar .)\n" +
                "He gets more points than I do.\n" +
                "(Benden daha çok sayı toplar.) \n" +
                "I enjoy playing with my best friend.\n" +
                "(En iyi arkadaşımla oynarken çok eğlenirim.)\n" +
                "I hope tomorrow will be the same forme and my best friend.\n" +
                "(Umarım yarında aynı formada ve yine en yakın arkadaşı olurum.) ");




        Intent intent = getIntent();

        String deger = intent.getStringExtra("bilgi");


        textView.setText(hashMap.get(deger));


    }
}
