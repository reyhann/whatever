package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class YorumEkle extends AppCompatActivity {
    ArrayList<String> yorumArray;
    ArrayList<Integer> idArray;
    ArrayList<String> baslikArray;
    ListView listView;
    ArrayAdapter arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yorum_ekle);
        yorumArray = new ArrayList<String>();
        idArray = new ArrayList<Integer>();
        baslikArray = new ArrayList<String>();
        listView = findViewById(R.id.listView);
        arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,baslikArray);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(YorumEkle.this,YorumYap.class);
                intent.putExtra("Id",idArray.get(position));
                intent.putExtra("info","old");


                startActivity(intent);
            }
        });

        getData();

    }


    public void getData(){

        try {

            SQLiteDatabase database = this.openOrCreateDatabase("Veritabani",MODE_PRIVATE,null);
            Cursor cursor = database.rawQuery("SELECT * FROM veritabani",null);
            int yorumIx = cursor.getColumnIndex("yorum");
            int idIx = cursor.getColumnIndex("id");
            int baslıkIx = cursor.getColumnIndex("baslik");

            while(cursor.moveToNext()){
                yorumArray.add(cursor.getString(yorumIx));
                idArray.add(cursor.getInt(idIx));
                baslikArray.add(cursor.getString(baslıkIx));
            }

            arrayAdapter.notifyDataSetChanged();

            cursor.close();

        }catch (Exception e){
            e.printStackTrace();
        }




    }


    public void ekle(View view){

        Intent intent = new Intent(YorumEkle.this,YorumYap.class);
        intent.putExtra("info","new");
        startActivity(intent);



    }



}
