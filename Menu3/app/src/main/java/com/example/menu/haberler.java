package com.example.menu;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class haberler extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haberler);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView hbr = (TextView) findViewById(R.id.haber);
        ImageView resim = (ImageView) findViewById(R.id.resimxxx);

        Intent mIntent = getIntent();
        int haberid = mIntent.getIntExtra("haberid", 0);

        if (haberid == 1) {
            resim.setBackground(getResources().getDrawable(R.drawable.rabiaaa));
            hbr.setText("\n Protests Swell in U.S. and Beyond as George Floyd Is Mourned Near His Birthplace\n" +
                    "\nDemonstrators massed around the world, and even small towns with Klan histories became sites of protest. The Minneapolis mayor was shouted down by protesters.\n" +
                    "Demonstrations across the United States, which began as spontaneous eruptions of outrage after the death of George Floyd at the hands of the Minneapolis police less than two weeks ago, appeared to have cohered by Saturday into a nationwide movement protesting systemic racism.\n" +
                    "Thousands marched in big cities like New York and Seattle, and tighter groups in small towns like Vidor, Texas; Havre, Mont.; and Marion, Ohio, denouncing a broken law enforcement system marked by racial injustice.\n");
        } else if (haberid == 2) {
            resim.setBackground(getResources().getDrawable(R.drawable.rabia));
            hbr.setText("\n N.Y. Protesters Defy Curfew, but 10th Night of Marches Ends Peacefully\n" +
                    "\n After several nights where the police moved aggressively, few arrests were reported Saturday, though protests continued till near midnight.\n" +
                    "Protest marches against racism and police brutality continued in New York City well past 11 p.m. on Saturday, defying an 8 p.m. curfew but allowed to continue peacefully by the police, who had moved aggressively to stop protests after curfew on recent nights.\n" +
                    "\n" +
                    "The biggest march of the night, which began at Barclays Center as curfew fell, with well over 1,000 people, made a jubilant 8-mile loop through the center of Brooklyn.\n" +
                    "\n");
        } else if (haberid == 3) {
            resim.setBackground(getResources().getDrawable(R.drawable.rabiaa));
            hbr.setText("\n Hundreds of thousands of straphangers are expected to return to the subway Monday as New York City’s coronavirus restrictions begin to loosen — and while the MTA is working to make sure trains as safe as it can, many riders still fear a mass spread of the disease on the system.\n" +
                    "\n" +
                    "\n “I’m worried about other people on the train without a mask. You don’t know who is sick,” said Ximena Bernal, 36, an interpreter for non-English speaking patients at NYU-Langone Hospital, which was on the front line of the pandemic.\n" +
                    "\n" +
                    "She rides the subway five days a week.");
        }
    }
}