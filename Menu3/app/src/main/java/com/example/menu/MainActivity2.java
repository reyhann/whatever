package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.HashMap;


public class MainActivity2 extends AppCompatActivity {
    TextView textView;
    HashMap<String, String> hashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        textView = findViewById(R.id.textView);


        hashMap = new HashMap<>();
        hashMap.put(" ", " ");
        hashMap.put("   A"," \n Apple : Elma \n Aback: Şaşkın \n Access: Geçiş \n abondon: terketmek \n ability: yetenek");
        hashMap.put("   B"," \n Banana: Muz \n baby: bebek \n back: Arka \n body: Vücut \n belive: inanmak");
        hashMap.put("   C"," \n Cartoon: Çizgi Film \n Curly: Kıvırcık \n Cut: Kesmek \n Cat: Kedi \n compact: Yoğun");
        hashMap.put("   D"," \n Dinner: akşam yemeği \n drop: damla \n duty: nöbet \n demon: şeytan \n depot: depo");
        hashMap.put("   E"," \n Elephant: fil \n Egg: Yumurta \n Ego: benlik\n eve: Havva \n eye: göz");
        hashMap.put("   F"," \n fail: başaramamak \n fair: adaletli \n fight: dövüş\n flash: şimşek \n fury: hiddet,öfke");
        hashMap.put("   G"," \n\n Gait: yürüyüş\n gap: aralık,boşluk \n groin: kasık\n gut: sindirim sistemi \n gym: spor salonu\n gyp:hile");
        hashMap.put("   H"," \n happy: mutlu \n handle: ele almak \n hazel: ela \n hew : yontmak\n hurt: incitmek");
        hashMap.put("   I-i"," \n ice: buz\n imp: küçük şeytan \n in: içinde \n install: kurmak \n ivory: fildişi");



        Intent intent = getIntent();

        String deger = intent.getStringExtra("bilgi");


        textView.setText(hashMap.get(deger));


    }
}
