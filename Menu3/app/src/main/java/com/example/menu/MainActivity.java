package com.example.menu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class MainActivity extends AppCompatActivity {
    Button linkbutton;

    CardView haber1,haber2,haber3;
    private Object ViewFlipper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linkbutton=(Button)findViewById(R.id.linkbutton);
        linkbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri linkimiz= Uri.parse("https://www.nytimes.com/");
                Intent intentimiz= new Intent(Intent.ACTION_VIEW,linkimiz);
                startActivity(intentimiz);

            }
        });

        android.widget.ViewFlipper flipper;
        ViewFlipper= flipper =(android.widget.ViewFlipper) findViewById(R.id.viewFlipper);
        haber1=(CardView) findViewById(R.id.haber1);
        haber2=(CardView) findViewById(R.id.haber2);
        haber3=(CardView) findViewById(R.id.haber3);

        haber1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this,haberler.class);
                myIntent.putExtra("haberid",1);
                startActivity(myIntent);
            }
        });
        haber2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this,haberler.class);
                myIntent.putExtra("haberid",2);
                startActivity(myIntent);
            }
        });
        haber3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this,haberler.class);
                myIntent.putExtra("haberid",3);
                startActivity(myIntent);
            }
        });
        flipper.setAutoStart(true);

    }
}