package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity3 extends AppCompatActivity {
    ListView listView;
    ArrayList<String> list;
    ArrayAdapter adapter;
    String kelime="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        listView = findViewById(R.id.listView1);

        list = new ArrayList<String>();
        list.add("");
        list.add("    Hello : Merhaba");
        list.add("    Welcome : Hoş geldiniz");
        list.add("    Have a good day = İyi günler");
        list.add("    Miss/ Mrs = Bayan");
        list.add("    Mr = Bay ");
        list.add("    Morning = Sabah");
        list.add("    Goodmorning = Günaydın");
        list.add("    Beautifull = Güzel");
        list.add("    But = Ama");
        list.add("    because = Çünkü");
        list.add("    lazy = tembel");
        list.add("    car : Araba");
        list.add("    horse : At");
        list.add("    banana : Muz");
        list.add("    apple : Elma");
        list.add("    city : Şehir");
        list.add("    water : Su");
        list.add("    father :Baba");
        list.add("    mother : Anne");
        list.add("    love : Aşk");
        list.add("    fire : Alev");
        list.add("    man : Adam");
        list.add("    woman : Kadın");


        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,list);
        listView.setAdapter(adapter);






    }
}
