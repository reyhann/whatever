package com.example.aramacubugu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SearchView mySearchView;
    ListView myList;

    ArrayList<String> list;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mySearchView= (SearchView)findViewById(R.id.searchView);
        myList= (ListView)findViewById(R.id.myList);

        list= new ArrayList<String>();

        list.add("apple ( elma )");
        list.add("banana ( muz )");
        list.add("love ( aşk )");
        list.add("sad ( üzgün )");
        list.add("girl ( kız )");
        list.add("boy ( erkek )");
        list.add("man ( adam )");
        list.add("side ( taraf )");
        list.add("like ( beğenmek )");
        list.add("woman ( kadın )");
        list.add("father ( baba )");
        list.add("lemon ( limon )");
        list.add("left ( sol )");
        list.add("right ( sağ )");
        list.add("happy ( mutlu )");
        list.add("book ( kitap )");
        list.add("pencil ( kalem )");
        list.add("year ( yıl )");
        list.add("window ( pencere )");
        list.add("hot ( sıcak )");
        list.add("mother ( anne )");
        list.add("uncle ( amca )");
        list.add("son ( oğul )");
        list.add("soon ( yakında )");
        list.add("baby ( bebek )");
        list.add("money ( para )");
        list.add("taste ( tatmak )");
        list.add("real ( gerçek )");
        list.add("rain ( yağmur )");
        list.add("salt ( tuz )");
        list.add("melon ( kavun )");
        list.add("watermelon ( karpuz )");
        list.add("water ( su )");
        list.add("back ( geri )");
        list.add("air ( hava )");
        list.add("toys ( oyuncak )");





        adapter= new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,list);
        myList.setAdapter(adapter);

        mySearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               adapter.getFilter().filter(newText);

                return false;
            }
        });

    }
}
