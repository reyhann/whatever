package com.example.kategorilendirilmiskelimeler;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class ikinci_haller extends AppCompatActivity {
    ListView liste;
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ikincihaller);
        liste=(ListView)findViewById(R.id.listView2);
        String[] veriler={"head--->baş","ear--->kulak","nose--->burun","mouth--->ağız","eyes--->gözle","finger--->parmak","leg--->diz","foot--->bacak",
                "give--->gave","hit--->hit","hold--->held","keep--->kept","lay--->laid","leave--->left","lose--->lost","meet--->met","ring--->rang",
        "hand--->el","cheek--->yanak","knee--->diz","arm--->kol","hair--->saç","lip--->dudak"};

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,android.R.id.text1,veriler);
        liste.setAdapter(adapter);
    }

}