package com.example.kategorilendirilmiskelimeler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button biregit=(Button) findViewById(R.id.bir);
        biregit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,birincihaller.class);
                startActivity(intent);
            }
        });
        Button ikiyegit=(Button) findViewById(R.id.iki);
        ikiyegit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ikinci_haller.class);
                startActivity(intent);
            }
        });
        Button ucegit=(Button) findViewById(R.id.uc);
        ucegit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ucuncuhaller.class);
                startActivity(intent);
            }
        });
    }
}
