package com.example.kategorilendirilmiskelimeler;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class ucuncuhaller extends AppCompatActivity {
    ListView liste;
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ucuncuhaller);
        liste=(ListView)findViewById(R.id.listView3);
        String[] veriler={"sunny--->güneşli","rainy--->yağmurlu","windy--->rüzgarlı","foggy--->sisli","snow--->kar","hail--->dolu","shower--->hafif yağmurlu","hot--->sıcak","cold--->soğuk",
                "lightning--->şimşek","rainbow--->gökkuşağı","hurricane--->fırtına"};

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,android.R.id.text1,veriler);
        liste.setAdapter(adapter);
    }

}
