package com.example.hangidilhangilke;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.HashMap;


public class MainActivity2 extends AppCompatActivity {
    TextView textView;
    HashMap<String, String> hashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        textView = findViewById(R.id.textView);


        hashMap = new HashMap<>();
        hashMap.put(" ", " ");
        hashMap.put("   Almanca"," \n Almanya \n" +
                "\n" +
                "Avusturya\n" +
                "\n" +
                "Belçika\n" +
                "\n" +
                "Danimarka\n" +
                "\n" +
                "İsviçre\n" +
                "\n" +
                "Liechtenstein\n" +
                "\n" +
                "Lüksemburg");
        hashMap.put("   Afrika Yerel Dilleri"," \n Namibia\n" +
                "\n" +
                "Zambia\n" +
                "\n" +
                "Arapça Konuşan Ülkeler\n" +
                "\n" +
                "Bahreyn\n" +
                "\n" +
                "Batı Sahra");
        hashMap.put("   Arapça"," \n Birleşik Arap Emirlikleri\n" +
                "\n" +
                "Cezayir\n" +
                "\n" +
                "Çad\n" +
                "\n" +
                "Fas\n" +
                "\n" +
                "Filistin\n" +
                "\n" +
                "Irak\n" +
                "\n" +
                "İsrail\n" +
                "\n" +
                "Katar\n" +
                "\n" +
                "Komor Adaları\n" +
                "\n" +
                "Kuveyt\n" +
                "\n" +
                "Libya\n" +
                "\n" +
                "Lübnan\n" +
                "\n" +
                "Mısır\n" +
                "\n" +
                "Moritanya\n" +
                "\n" +
                "Somali\n" +
                "\n" +
                "Sudan\n" +
                "\n" +
                "Suriye\n" +
                "\n" +
                "Suudi Arabistan\n" +
                "\n" +
                "Tunus\n" +
                "\n" +
                "Umman\n" +
                "\n" +
                "Ürdün\n" +
                "\n" +
                "Yemen\n" +
                "\n" +
                "Eritre\n" +
                "\n" +
                "Tanzanya");
        hashMap.put("   Azerice"," \n Azerbaycan \n" +
                "\n" +
                "Ermenistan\n" +
                "\n" +
                "İran");
        hashMap.put("   Çince"," \n Çin\n" +
                "\n" +
                "Tayvan");
        hashMap.put("   Danimarkaca"," \n Danimarka\n" +
                "\n" +
                "Grönland");
        hashMap.put("   Ermenice"," \n\n Ermenistan\n" +
                "\n" +
                "İran\n" +
                "\n" +
                "Lübnan\n" +
                "\n" +
                "Suriye");
        hashMap.put("   Farsça"," İran\n" +
                "\n" +
                "Irak\n" +
                "\n" +
                "Katar\n" +
                "\n" +
                "Tacikistan");
        hashMap.put("   Fransızca"," \n Andorra\n" +
                "\n" +
                "Benin\n" +
                "\n" +
                "Burkina\n" +
                "\n" +
                "Çad\n" +
                "\n" +
                "Dibuti\n" +
                "\n" +
                "Fransa\n" +
                "\n" +
                "Fransız Guyanası\n" +
                "\n" +
                "Fransız Polinezyası\n" +
                "\n" +
                "Gabon\n" +
                "\n" +
                "Gine\n" +
                "\n" +
                "Haiti\n" +
                "\n" +
                "İsviçre\n" +
                "\n" +
                "İtalya\n" +
                "\n" +
                "Kanada\n" +
                "\n" +
                "Kongo\n" +
                "\n" +
                "Lüksemburg\n" +
                "\n" +
                "Madagaskar\n" +
                "\n" +
                "Mali\n" +
                "\n" +
                "Martinik\n" +
                "\n" +
                "Monako\n" +
                "\n" +
                "Nijer\n" +
                "\n" +
                "Orta Afrika Cumhuriyeti\n" +
                "\n" +
                "Ruanda\n" +
                "\n" +
                "Senegal\n" +
                "\n" +
                "Togo\n" +
                "\n" +
                "Wallis ve Futuna\n" +
                "\n" +
                "Yeni Kaleydonya\n" +
                "\n" +
                "Zaire\n" +
                "\n" +
                "Belçika\n" +
                "\n" +
                "Cezayir\n" +
                "\n" +
                "Lübnan\n" +
                "\n" +
                "Tunus");
        hashMap.put("   İngilizce"," Amerika Birleþik Devletleri\n" +

                "Anguilla\n" +

                "Antigua ve Barbados\n" +

                "Avustralya\n" +

                "Bahamalar\n" +

                "Belize\n" +

                "Bermuda\n" +

                "Birleşik Krallık\n" +

                "Botswana\n" +

                "Dominikya\n" +

                "Eritre\n" +

                "Etyopya\n" +

                "Falkland Adaları\n" +

                "Fiji\n" +

                "Filipinler\n" +

                "Gambia\n" +

                "Gana\n" +

                "Gibraltar\n" +

                "Granada\n" +

                "Guam\n" +

                "Guernsey\n" +

                "Guyana\n" +

                "Güney Afrika\n" +

                "Hindistan\n" +

                "İngiliz Virgin Adaları\n" +

                "İrlanda\n" +

                "İsrail\n" +

                "Jamaika\n" +

                "Jersey\n" +

                "Kamerun\n" +

                "Kanada\n" +

                "Kenya\n" +

                "Kiribati\n" +

                "Kokolar\n" +

                "Kook Adaları\n" +

                "Kuzey Mariana Adaları\n" +

                "Lesotho\n" +

                "Liberya\n" +

                "Malawi\n" +

                "Malta\n" +

                "Mauritius\n" +

                "Mikronezya\n" +

                "Montserrat\n" +

                "Namibia\n" +

                "Nijerya\n" +

                "Norfolk Adaları\n" +

                "Pakistan\n" +

                "Palau\n" +

                "Papua Yeni Gine\n" +

                "Pitcairn Adaları\n" +

                "Ruanda\n" +

                "Saint Helena\n" +

                "Seyþel Adaları\n" +

                "Singapur\n" +

                "Sierra Lone\n" +

                "Somali\n" +

                "Swaziland\n" +

                "Trinidad ve Tobago\n" +

                "Türk ve Kafkas Adaları\n" +

                "Uganda\n" +

                "Vanatu\n" +

                "Zambia\n" +

                "Zimbabve\n" +

                "Marshall Adaları\n" +

                "İspanyolca Konuşan Ülkeler\n" +

                "Arjantin\n" +

                "Bolivya\n" +

                "Dominik Cumhuriyeti\n" +

                "Ekvador\n" +

                "Ekvador Ginesi\n" +

                "El Salvador\n" +

                "Guatemala\n" +

                "Honduras\n" +

                "İspanya\n" +

                "Kolombiya\n" +

                "Kosta Rika\n" +

                "Küba\n" +

                "Meksika\n" +

                "Nikaragua\n" +

                "Panama\n" +

                "Paraguay\n" +

                "Peru\n" +

                "Porto Riko\n" +

                "Şili\n" +

                "Uruguay\n" +

                "Venezuela\n" +

                "Andorra\n" +

                "Belize\n" +

                "Fas\n" +

                "Filipinler");
        hashMap.put("   İtalyanca"," \n Arjantin\n" +
                "\n" +
                "İtalya\n" +
                "\n" +
                "Hırvatistan\n" +
                "\n" +
                "San Marino\n" +
                "\n" +
                "İsviçre\n" +
                "\n" +
                "Slovenya\n" +
                "\n" +
                "Eritre");
        hashMap.put("   Korece"," \n Güney Kore\n" +
                "\n" +
                "Kuzey Kore\n" +
                "\n" +
                "Lao Konuşan Ülkeler\n" +
                "\n" +
                "Laolar");


        Intent intent = getIntent();

        String deger = intent.getStringExtra("bilgi");


        textView.setText(hashMap.get(deger));


    }
}
